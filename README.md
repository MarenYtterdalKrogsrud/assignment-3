RPG

This program is a RPG character system.
Each Hero has several attributes and a level. 
To get the Hero up to a certain level they need experience or(XP). There is a 
method in the system that makes this possible. Each Hero starts at 0 experience 
and they need 100 to get to Level 2. The required experience to get to the next level is increased by 10% each time. 

There are three different Hero types: Ranger, Warrior and Mage. They start with different attributes and when the Hero goes up one level they gain point to the attributes. This also differ from hero type to hero type.

Items

The heroes have the possibility to equip different types of items. There are armors and weapons. When a Hero puts on an armor it can affect the attribute stats andthe different stats changes depending on the type of armor. 

The armor types can be:
- Cloth
- Leather
- Plate

Slots

These armor types can be in different slots: body, head or legs. If the slot is legs it will scale the number of calculated stats for the item by 60%. If it is a head slot it will scale the number of calculated stats for the item by 80%. The body slot will give 100% of the calculated stats for the item.

Weapon

There are three types of weapon: melee, ranged and magic. Weapons has a base damage which is increased by dexterity for ranged, strength for melee and intelligence for magic when calculated damage dealt. The damage is also scaled after the level the weapon level. 

When a hero attacks it will calculate the damage dealt.

In the main I have created several examples to display certain functonalities:
 
- Generates two heroes: warrior1 and ranger1.
- Give them experience by addXP method.
- Created three type for armors:
 * Plate, body
 * Cloth, legs
 * Leather, head
- Equip warrior1 with armor2
- Remove armor from warrior1
- Create a weapon1: Melee weapon 
  - Warrior1 equip weapon1 
  - Warrior1 attack with weapon1
-	remove weapon1 from warrior1




