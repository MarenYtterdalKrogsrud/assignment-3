package com.company.Items;


public class Armor {

    public final ArmorType armortype;
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;
    public int armorLevel;
    public SlotType slotType;
    protected String armorName;

    // public Armor takes the armor from the enum ArmorType class as well as the other variables stated in the main.
    public Armor(ArmorType armortype, String armorName, SlotType slotType, int armorLevel){ //add info to method
        this.armortype = armortype;
        this.armorName = armorName;
        this.armorLevel = armorLevel;
        this.slotType = slotType;

        switch(armortype) { // a switch case for the armortype defined to add different
            case CLOTH:     // stats values as these are different from armortype to armortype
                health = 10;
                dexterity = 1;
                intelligence = 3;
                extraLevelPoints(); // calls method to add extra points for new level
                slotScale(); // calls the slotScale method to calculate the scale
                break;
            case LEATHER:
                health = 20;
                strength = 1;
                dexterity = 3;
                extraLevelPoints();
                slotScale();
                break;
            case PLATE:
                health = 30;
                strength = 3;
                dexterity = 1;
                extraLevelPoints();
                slotScale();
                break;
        }
    }

    // This method is to add extra points to the stats and uses a switch case as this differ from armortype to armortype
    // as this only is used after level 1 the counter variable below is defined as one.
    public void extraLevelPoints() {
        int count = 1;
        while (count < armorLevel) {  // check if the armour level is more then then count as we need to loop through as
            switch (armortype) {      // it scales per level
                case CLOTH:
                    health += 5;
                    dexterity += 1;
                    intelligence += 2;
                    count++;        // add counts
                    break;
                case LEATHER:
                    health += 8;
                    strength += 1;
                    dexterity += 2;
                    count++;
                    break;
                case PLATE:
                    health += 12;
                    strength += 2;
                    dexterity += 1;
                    count++;
                    break;
            }
        }
    }

    // This method is used to calculate the correct slot scale.
    // This this is only for leg and head as body has 100% of the amount calculated
    public void slotScale(){
       if(slotType.equals(SlotType.HEAD)) {
           intelligence = (int) (intelligence * (80.0/100.0));
           dexterity = (int) (dexterity * (80.0f / 100.0f));
           health = (int) (health * (80.0f / 100.0f));
           strength = (int) (strength * (80.0f / 100.0f));
       }
       if(slotType.equals(SlotType.LEGS)){
            health = (int) (health * (60.0f/100.0f));
            strength = (int) (strength * (60.0f/100.0f));
            dexterity = (int)(dexterity * (60.0f/100.0f));
            intelligence = (int) (intelligence * (60.0f/100.0f));
       }else{
           return;
       }

    }

    // this method is to print out the stats for the Armor
    // As the stats differ from armor to armor the if is to display the correct
    // output of the different stats
    public String printStatsArmor() {
        StringBuilder builder = new StringBuilder();
        builder.append("Item stats for : " + armorName);
        builder.append("\nArmor Type: " + armortype);
        builder.append("\nSlot: " + slotType);
        builder.append("\nArmor level: " + armorLevel);
        builder.append("\nBonus HP: " + health);
        if (armortype == ArmorType.LEATHER || armortype == ArmorType.PLATE){
            builder.append("\nBonus Str: " + strength);
        }
        builder.append("\nBonus Dex: "+dexterity);
        if(armortype == ArmorType.CLOTH) {
            builder.append("\nBonus Int: " + intelligence);
        }
        return builder.toString();
    }
}
