package com.company.Items;

public class Weapon {
        public WeaponType weapontype;
        public int weaponLevel;
        public int damage;
        protected String weaponName;

        // This takes the weapontype from the enum class WeaponType with the level of the weapon and
        // the weapon name defined in the main
        public Weapon(String weaponName, WeaponType weapontype, int weaponLevel){ //add info to method
            this.weapontype = weapontype;
            this.weaponLevel = weaponLevel;
            this.weaponName = weaponName;

            switch(weapontype) {  // The switch case is to check the weapontype defined and add
                case MELEE:       // correct amount of points to the damage
                    damage = 15;
                    baseDamageLevelCalc(); // calls the method where the damage is calculated
                    break;
                case RANGED:
                    damage = 5;
                    baseDamageLevelCalc();
                    break;
                case MAGIC:
                    damage = 25;
                    baseDamageLevelCalc();
                    break;
            }
        }

        // This method is to calculate the damage based on the level
        public void baseDamageLevelCalc() {
            int count = 1;
            while (count < weaponLevel) {  // check if the armour level is more then then count as we need to loop through as
                switch (weapontype) {      // it scales per level
                    case MELEE: // magic and melee scales the same
                    case MAGIC:
                        damage +=2;
                        count++;
                        break;
                    case RANGED:
                        damage +=3;
                        count++;
                        break;
                }
            }
        }

        //this method is to print out the stats of the weapon
        public String printStatsWeapon(){
            StringBuilder builder = new StringBuilder();
            builder.append("Item stats for: " + weaponName);
            builder.append("\nArmortype: " + weapontype);
            builder.append("\nWeapon level: " + weaponLevel);
            builder.append("\nDamage: " + damage);
            return builder.toString();
        }
}
