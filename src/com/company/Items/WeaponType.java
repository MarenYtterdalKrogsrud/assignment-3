package com.company.Items;

public enum WeaponType {
  MELEE,
  RANGED,
  MAGIC
}
