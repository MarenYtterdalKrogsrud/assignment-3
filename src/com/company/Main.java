package com.company;

import com.company.Hero.Hero;
import com.company.Hero.HeroType;
import com.company.Items.*;

public class Main {
    public static void main(String[] args) {

        //creating a hero with the name warrior1. Defined as a herotype: Warrior
        Hero warrior1= new Hero(HeroType.WARRIOR);
        System.out.println(warrior1.printStats());
        warrior1.addXP(210); //add 210 XP so the warrior goes up to level 3
        System.out.println(warrior1.printStats());

        //creating a hero with the name ranger1. Defined as a herotype: Ranger
        Hero ranger1= new Hero(HeroType.RANGER);
        System.out.println(ranger1.printStats());
        ranger1.addXP(331); //add 210 XP so the warrior goes up to level 4
        System.out.println(ranger1.printStats());

        //creating an armor with the name armor1. This is a body plate and has a level 2
        Armor armor1 = new Armor(ArmorType.PLATE,"Chest plate of a undead warrior ", SlotType.BODY,2);
        System.out.println(armor1.printStatsArmor());

        //creating an armor with the name armor2. This is a cloth leggings and has a level 4
        Armor armor2 = new Armor(ArmorType.CLOTH, "Robin hoods lost trousers ", SlotType.LEGS, 4);
        System.out.println(armor2.printStatsArmor());

        //creating an armor with the name armor3. This is a leather head with a level 5
        Armor armor3 = new Armor(ArmorType.LEATHER, "Helmet made out of cow ", SlotType.HEAD, 5);
        System.out.println(armor3.printStatsArmor());

        //give ranger1 an armor (armor1)
        System.out.println(ranger1.printStats());
        System.out.println(armor1.printStatsArmor());
        ranger1.equipArmor(armor1);
        System.out.println(ranger1.printStats());

        //give warrior1 an armor (armor2) which will not pass as warrior1 does not have the right level
        System.out.println(warrior1.printStats());
        System.out.println(armor2.printStatsArmor());
        warrior1.equipArmor(armor2);
        System.out.println(warrior1.printStats());

        //remove armor1 from ranger1
        System.out.println(ranger1.printStats());
        ranger1.removeArmor(SlotType.BODY);
        System.out.println(ranger1.printStats());

        //create a weapon
        Weapon weapon1 = new Weapon("The great axe ", WeaponType.MELEE, 2 );
        System.out.println(weapon1.printStatsWeapon());

        //Give warrior1 weapon1
        warrior1.equipWeapon(weapon1);

        //Demonstrates warrior1 attack with weapon1
        warrior1.attack();

        //remove weapon from warrior
        warrior1.removeWeapon();

        //try to attack without weapon
        warrior1.attack();
    }
}
