package com.company.Hero;
import com.company.Items.*;

public class Hero {
        protected int health;
        protected int strength;
        protected int dexterity;
        protected int intelligence; // these variables are protected as these should not be accessed
        protected int experience;   // outside the class
        protected int level;
        protected Weapon equippedWeapon;
        protected Armor equippedHeadArmor;
        protected Armor equippedBodyArmor;
        protected Armor equippedLegsArmor;
        protected final HeroType herotype;
        protected double exAmount = 100;

        public Hero(HeroType herotype){ // public hero takes the herotype from the enum HeroType class
            this.herotype = herotype;
            level = 1;  // define level as 1 as Heroes starts at level 1
            experience = 0; // define experience as the Hero needs experience points to reach to certain levels

            switch(herotype) { // a switch case for the herotypes defined to add different
                case WARRIOR:  // stats values as these are different from herotype to herotype
                    health = 150;
                    strength = 10;
                    dexterity = 3;
                    intelligence = 1;
                    break;
                case RANGER:
                    health = 120;
                    strength = 5;
                    dexterity = 10;
                    intelligence = 2;
                    break;
                case MAGE:
                    health = 100;
                    strength = 2;
                    dexterity = 3;
                    intelligence = 10;
                    break;
            }
        }

        // this method is to increase stats when a hero reach a new level
        // as this also differ from herotype to herotype
        public void levelUpPoints() {
            switch (herotype) {
                case WARRIOR:
                    health += 30;
                    strength += 5;
                    dexterity += 2;
                    intelligence += 1;
                    level++; // this is to add the level to the hero
                    break;
                case RANGER:
                    health += 20;
                    strength += 2;
                    dexterity += 5;
                    intelligence += 1;
                    level++;
                    break;
                case MAGE:
                    health += 15;
                    strength += 1;
                    dexterity += 2;
                    intelligence += 5;
                    level++;
                    break;
            }
        }

        //this method is to give the hero more experience to reach a new level
        //as well as then calculate the level for the hero
        public void addXP(int XP) { //XP the amount of experience point the user wants to give
            experience += XP; //adds this to the experience variable.
            if (experience >= exAmount){ // test if the experience the hero have received is more then the
                while (experience >= exAmount) {   // ex amount the hero needs which is set to 100 as this is the amount to get to level 2
                    levelUpPoints();                // calls this method to add stats when reached new level
                    exAmount = (int)(exAmount + (100 * (Math.pow(1.1, level -1)))); //I know you didnt want this but I could find another solution, sorrryy
                }
            }
        }

        // this method is to equip the hero with a weapon an assign the weapon tp the hero
        // it also checks if the hero have the right level to use the weapon
        public void equipWeapon(Weapon weapon){
            if (level <  weapon.weaponLevel) {
                System.out.println("Sorry, you need to be more experienced to equip this weapon");
                return;
            }
            equippedWeapon = weapon;
        }

        // this method is used when the hero attacks
        // the switch case is used to calculate the amount of damage dealt when attacking as this differ from different heroes
        public void attack(){
            int damageDealt;
            if(equippedWeapon == null){ //to check if the hero has a weapon as it cant attack without a weapon.
                System.out.println("Hello I need a weapon!");
                return;
            }
            switch(equippedWeapon.weapontype) {
                case MELEE:
                    damageDealt = (int) ((strength * 1.5) + equippedWeapon.damage); //calculation of damage
                    break;
                case RANGED:
                    damageDealt = (int) ((dexterity * 2) + equippedWeapon.damage);
                    break;
                case MAGIC:
                    damageDealt = (int) ((intelligence * 3) + equippedWeapon.damage);
                    break;
                default:
                    damageDealt = 0;
            }
            System.out.println("Attacking for " + damageDealt);
        }

        // method to remove the weapon as they cant use more then one at the same time
        public void removeWeapon(){
            equippedWeapon = null;
        }

        // this method sends the armor the hero wants to use and first checks if the hero has the right level to use it.
        // Then the switch case is to check which slottype it is as the hero can have all the different type on at the same time.
        public void equipArmor(Armor armor) {
            if (level < armor.armorLevel) {
                System.out.println("Sorry, you need to be more experienced to use this!");
                return;
            }
            switch (armor.slotType) {
                case BODY:
                    if(equippedBodyArmor != null){ //check if the hero already has armor for this slot
                        removeEquipmentStats(equippedBodyArmor);
                    }
                    equippedBodyArmor = armor;
                    break;
                case HEAD:
                    if(equippedHeadArmor != null){
                        removeEquipmentStats(equippedHeadArmor);
                    }
                    equippedHeadArmor = armor;
                    break;
                case LEGS:
                    if(equippedLegsArmor != null){
                        removeEquipmentStats(equippedLegsArmor);
                    }
                    equippedLegsArmor = armor;
                    break;
            }
            addEquipmentStats(armor);  // calls the method to add the stats after the hero has equipped the slot
        }

        // This method is similar to the one above just that here it removes the slottype by first checking the slottype
        public void removeArmor(SlotType armorSlot){
            switch (armorSlot) {
                case BODY:
                    if(equippedBodyArmor != null){
                        removeEquipmentStats(equippedBodyArmor);
                        equippedBodyArmor = null;
                    }
                    break;
                case HEAD:
                    if(equippedHeadArmor != null){
                        removeEquipmentStats(equippedHeadArmor);
                        equippedHeadArmor = null;
                    }
                    break;
                case LEGS:
                    if(equippedLegsArmor != null){
                        removeEquipmentStats(equippedLegsArmor);
                        equippedLegsArmor = null;
                    }
                    break;
            }
        }

        // this is to change the stats to the Hero when user remove an armor
        protected void removeEquipmentStats(Armor armor){
            health -= armor.health;
            intelligence -= armor.intelligence;
            dexterity -= armor.dexterity;
            strength -= armor.strength;
        }

        // this method adds the stats to the hero
        protected void addEquipmentStats(Armor armor){
            health += armor.health;
            intelligence += armor.intelligence;
            dexterity += armor.dexterity;
            strength += armor.strength;

        }

        // this method returns the details of the hero
        public String printStats(){
            StringBuilder builder = new StringBuilder();
            builder.append(herotype + " details:");
            builder.append("\nHP: "+health);
            builder.append("\nStr: "+strength);
            builder.append("\nDex: "+dexterity);
            builder.append("\nInt: "+intelligence);
            builder.append("\nLvl: "+level);
            builder.append("\nXP:" + experience);
            return builder.toString();
        }
}
